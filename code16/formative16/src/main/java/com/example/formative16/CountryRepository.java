package com.example.formative16;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CountryRepository extends CrudRepository<Country, Integer> {
    Country findById(int id);
    List<Country> findAll();
    void deleteById(Country id);
    Country save(Country country);
}
