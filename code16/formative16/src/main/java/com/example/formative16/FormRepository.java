package com.example.formative16;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FormRepository extends CrudRepository<Price, Integer> {
    Price findById(int id);
    List<Price> findAll();
    void deleteById(Price id);
    Price save(Price price);
}
