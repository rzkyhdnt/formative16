package com.example.formative16;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {
    @Autowired
    FormRepository formRepository;

    @Autowired
    PriceDAO priceDAO;

    @Autowired
    CountryRepository countryRepository;

    public List<Country> getAllCountries(){
        return countryRepository.findAll();
    }

    public List<Price> getAllPrice(){
        return formRepository.findAll();
    }


    @ModelAttribute
    public void addCountry(Model model){
        List<Country> countryList = new ArrayList<>(getAllCountries());
        model.addAttribute("countries", countryList);
    }

    @ModelAttribute
    public void addPrice(Model model){
        List<Price> priceList = new ArrayList<>(getAllPrice());
        model.addAttribute("prices", priceList);
    }
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id, Model model){
        Price product = formRepository.findById(id);
        formRepository.delete(product);
        return "redirect:/show";
    }

    @GetMapping("/save")
    public String save(Price price){
        priceDAO.save(price);
        return "redirect:/show";
    }

    @GetMapping("/details/{id}")
    public String showDetails(@PathVariable("id") int id, Model model){
        Price price = formRepository.findById(id);
        model.addAttribute("price", price);
        return "details";
    }


    @GetMapping("/form")
    public String createPrice(Model model){
        Price price = new Price();
        model.addAttribute("price", price);
        return "form";
    }

    @GetMapping("/show")
    public String showProduct(Model model){
        return "show";
    }

    @PostMapping("/form")
    public String createPrice(@ModelAttribute("price") Price price) {
        formRepository.save(price);
        return "success";
    }


}
